<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Stevebauman\Location\Facades\Location;
use App\Models\Cuti;
use App\Models\Pegawai;
use App\Models\Ruangan;
use App\Models\SisaCuti;
use App\Models\V_Cuti;
use App\Models\KuotaCuti;
use App\Models\TransCuti;
use App\Models\Token;
use App\Models\User;
use App\Models\EsignCuti;
use carbon\carbon;
use DateTime;
use PDF;
use Auth;


class CutiController extends Controller
{
    //----------- ADMIN CONTROLLER ----------------

    //Menampilkan data cuti baru
    public function cutiAdmin()
    {
        $cuti = Cuti::where('Status', 'Disetujui')->orderBy('updated_at', 'desc')->get();
        return view('admin.datacuti.datacuti', compact('cuti'));
    }

    //Menampilkan detil cuti
    public function detil($id)
    {
        $detil = Cuti::where('id', $id)->get();
        $karu = Pegawai::where('NIP', $detil[0]['Atasan_Langsung'])->get();
        $kabid = Pegawai::where('NIP', $detil[0]['Kabid'])->get();
        return view('admin.datacuti.detilcuti', compact('detil','karu','kabid'));
    }

    //Konfirmasi admin - proses selesai
    public function konfirmasi(Request $request, $id)
    {
        $cuti = Cuti::where('id', $id)->first();
        $cuti->konfirmasi = 'Selesai';
        $cuti->admin = Auth::user()->name;
        $cuti->save();
        return redirect('/admin.datacuti')->with('success','Pengajuan Cuti Telah Dikonfirmasi');
    }

    //Cetak pengajuan cuti
    public function cetak($id)
    {
        $cuti = Cuti::where('id', $id)->get();
        $pegawai = Pegawai::where('IdPegawai', $cuti[0]['IdPegawai'])->get();
        $kabid = Pegawai::where('NIP', $cuti[0]['Kabid'])->get();
        $esign = EsignCuti::where('IdCuti', $id)->get();
        $qrcode = base64_encode(QrCode::format('svg')->size(70)->setLogo("images/logo-rs.png")->setLogoSize(98)
        ->setImageType(QrCode::IMAGE_TYPE_PNG)->errorCorrection('H')->generate($esign[0]['esign1']));
        $pdf = PDF::loadview('admin.datacuti.cetak', compact('cuti', 'pegawai', 'kabid', 'esign', 'qrcode'));
        return $pdf->stream();
    }

     //Kuota Cuti
     public function kuota()
     {
        $kuotacuti = KuotaCuti::orderby('Tahun', 'DESC')->limit(5)->get();
        return view ('admin.datacuti.kuotacuti', compact('kuotacuti'));
     }

     public function store(){
        $end = Carbon::now('Asia/Jakarta')->lastOfYear()->endOfDay();

        KuotaCuti::insert([
            'Tahun' => request('Tahun'),
            'Jumlah' => request('Jumlah'),
            'NO_SK' => request('NO_SK'),
            'Tgl_Kadaluarsa' => $end,
            'Status_Enabled' => 1,
            'created_at' => Carbon::now ('Asia/Jakarta')
        ]);

        $sisacuti = SisaCuti::all();
        $tahun_ambil = Carbon::now ('Asia/Jakarta')->format('Y');

        $N = 0;
        $N1 = 0;
        $N2 = 0;
        $setN1 = 0;
        $setN2 = 0;
        $selisihN = 0;

        foreach ($sisacuti as $sisa){
            $N = $sisa['Sisa_CutiN'];
            $N1 = $sisa['Sisa_CutiN1'];
            $N2 = $sisa['Sisa_CutiN2'];

            if($N2 > 0){
                TransCuti::insert([
                    'Id_DataCuti' => 0,
                    'IdPegawai'   => $sisa['IdPegawai'],
                    'Cuti_Ambil'  => $N2,
                    'Tahun_Ambil' => $tahun_ambil,
                    'Tahun_Jatah' => $tahun_ambil - 3,
                    'created_at'  => $date = Carbon::now ('Asia/Jakarta')
                ]);

                $setN2 = 6;
                $setN1 = 6;

                TransCuti::insert([
                    'Id_DataCuti' => 0,
                    'IdPegawai'   => $sisa['IdPegawai'],
                    'Cuti_Ambil'  => 6,
                    'Tahun_Ambil' => $tahun_ambil,
                    'Tahun_Jatah' => $tahun_ambil - 1,
                    'created_at'  => $date = Carbon::now ('Asia/Jakarta')
                ]);

            }elseif($N2 <= 0){
                if($N1>0){
                    $setN2 = $N1;
                    $setN1 = 6;

                    TransCuti::insert([
                        'Id_DataCuti' => 0,
                        'IdPegawai'   => $sisa['IdPegawai'],
                        'Cuti_Ambil'  => 6,
                        'Tahun_Ambil' => $tahun_ambil,
                        'Tahun_Jatah' => $tahun_ambil - 1,
                        'created_at'  => $date = Carbon::now ('Asia/Jakarta')
                    ]);
                }elseif($N1 <= 0){
                    $setN2 = 0;

                    if($N > 6){
                        $setN1 = 6;
                        $selisihN = $N - 6;

                        TransCuti::insert([
                            'Id_DataCuti' => 0,
                            'IdPegawai'   => $sisa['IdPegawai'],
                            'Cuti_Ambil'  => $selisihN,
                            'Tahun_Ambil' => $tahun_ambil,
                            'Tahun_Jatah' => $tahun_ambil -1,
                            'created_at'  => $date = Carbon::now ('Asia/Jakarta')
                        ]);
                    }elseif($N > 0 && $N <= 6){
                        $setN1 = $N;
                    }else{
                        $setN1 = 0;
                    }
                }
            }

            SisaCuti::where(['IdPegawai'=>$sisa['IdPegawai']])->update([
                'Sisa_CutiN' => request('Jumlah'),
                'Sisa_CutiN1' => $setN1,
                'Sisa_CutiN2' => $setN2
            ]);
        };

        return redirect()->back()->with('success','Kuota Cuti Created Successfully.');
     }

     //Update data sisa cuti
     public function updateKuota(Request $request, $id)
     {
         if ($request->isMethod('post')){
             $data = $request->all();

            $now = date('Y', strtotime(Carbon::now('Asia/Jakarta')));
            $sisacuti = SisaCuti::all();
            $kuotaN = KuotaCuti::where('Tahun', $now)->pluck('Jumlah');

            if ($kuotaN[0] == $data['Jumlah']){
                return redirect()->back()->with('failed', 'Data Sama Dengan Data Sebelumnya.');
            }else{
                foreach ($sisacuti as $sisa){
                    $selisih = $kuotaN[0] - $sisa['Sisa_CutiN'];
                    $SisaBaru = $data['Jumlah'] - $selisih;

                    SisaCuti::where(['IdPegawai'=>$sisa['IdPegawai']])->update([
                        'Sisa_CutiN' => $SisaBaru,
                    ]);

                }

                KuotaCuti::where(['id'=>$id])->update([
                    'Jumlah'=>$data['Jumlah'],
                    'NO_SK'=>$data['NO_SK'],
                ]);
            }
        }
            return redirect()->back()->with('success', 'Kuota Cuti Updated Successfully');
     }

    //Delete data cuti yang masih diproses
    public function destroy($id)
    {
        // $cuti = KuotaCuti::where(['id'=>$id])->delete();

        $cuti = KuotaCuti::where(['id'=>$id])->update([
            'Status_Enabled'=>'0'

        ]);

        //Check data deleted or not
        if ($cuti == 1){
            $success = true;
            $message = "Kuota Cuti Berhasil Dihapus";
        } else {
            $success = true;
            $message = "Kuota Cuti Tidak Ditemukan";
        }

        //Return response
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }

    //----------- USER CONTROLLER ----------------

    //Menampilkan data cuti
    public function cutiUser(){
        $id = Auth::user()->IdPegawai;
        $cuti = Cuti::where([['IdPegawai', $id],['Status_Enabled', 1]])->orderBy('id', 'desc')->limit(20)->get();
        $esign = EsignCuti::where('IdCuti', $id)->get();
        $data = array($cuti, $esign);
        // dd($cuti, $esign, $data);
        return view('user.cuti.datacuti', compact('cuti', 'esign', 'data'));
    }

    //Menampilkan form tambah cuti
    public function tambahCuti()
    {
        $id = Auth::user()->IdPegawai;
        $pegawai = Pegawai::where('IdPegawai', $id)->get();
        $ruangan = Ruangan::all();
        $sisacuti = SisaCuti::where('IdPegawai', $id)->get();
        $karu = User::where('roles_id', '3')->get();
        $kabid = Pegawai::where('Jenis_Pegawai', 'ASN')->get();

        return view('user.cuti.tambahcuti', compact('pegawai', 'ruangan', 'sisacuti', 'karu', 'kabid'));
    }

    //Detil cuti
    public function detilCuti($id)
    {
        $idpegawai = Auth::user()->IdPegawai;
        $detil = Cuti::where('id', $id)->get();
        $ruangan = Ruangan::all();
        $karu = Pegawai::where('NIP', $detil[0]['Atasan_Langsung'])->get();
        $kabid = Pegawai::where('NIP', $detil[0]['Kabid'])->get();
        $karulist = User::where('roles_id', 3)->get();
        $kabidlist = Pegawai::where('Jenis_Pegawai', 'ASN')->get();
        return view ('user.cuti.detilcuti', compact('detil', 'ruangan', 'karu', 'karulist', 'kabid', 'kabidlist'));
    }

    //Store Cuti
    public function storeCuti(Request $request)
    {
        $data = $request->all();

        //Menghitung jumlah hari
        $fdate = $request->Tgl_Mulai;
        $tdate = $request->Tgl_Selesai;
        $datetime1 = new DateTime($fdate);
        $datetime2 = new DateTime($tdate);
        $interval = $datetime2->diff($datetime1);
        $days = $interval->days;
        $jumlah = $days + 1;
        // dd($datetime1, $datetime2, $interval, $jumlah);

        $sisa = SisaCuti::where('IdPegawai', $data['IdPegawai'])->get();
        $sisacuti = $sisa[0]['Sisa_CutiN'] + $sisa[0]['Sisa_CutiN1'] + $sisa[0]['Sisa_CutiN2'];

        $tgl_tmt = new DateTime($request->Tgl_TMT);
        $today = new DateTime("today");

        $y = $today->diff($tgl_tmt)->y;
        $m = $today->diff($tgl_tmt)->m;
        $d = $today->diff($tgl_tmt)->d;

        //Menghitung jumlah cuti yg belum diproses
        $cuti = Cuti::where([['IdPegawai', $data['IdPegawai']],['Status_Enabled', 1],['Jenis_Cuti', 'Tahunan'],['Status', 'Belum Diproses']]);
        // dd($cuti->count(), $jumlah, $sisacuti);

        //Filter tanggal mulai dan akhir untuk pengajuan cuti harus ditahun yg sama
        // dd(date('Y', strtotime($data['Tgl_Mulai'])) == $now);

        if ($cuti->count() == 0){
            if ($data['Jenis_Cuti'] == 'Melahirkan'){
                Cuti::insert([
                    'IdPegawai'         => $data['IdPegawai'],
                    'NIP'               => $data['NIP'],
                    'Nama_Pegawai'      => $data['Nama_Pegawai'],
                    'Jabatan_Pegawai'   => $data['Jabatan_Pegawai'],
                    'Jenis_Pegawai'     => $data['Jenis_Pegawai'],
                    'Kategori_Pegawai'  => $data['Kategori_Pegawai'],
                    'Unit_Kerja'        => $data['Unit_Kerja'],
                    'Masa_Kerja'        => $y ." Tahun ". $m ." Bulan ". $d ." Hari ",
                    'Jenis_Cuti'        => $data['Jenis_Cuti'],
                    'Jumlah'            => 60,
                    'Tgl_Mulai'         => $data['Tgl_Mulai'],
                    'Tgl_Selesai'       => date('Y-m-d', strtotime('+59 days', strtotime($data['Tgl_Mulai']))),
                    'No_Telp'           => $data['No_Telp'],
                    'Pengalihan_Kerja'  => $data['Pengalihan_Kerja'],
                    'Alasan'            => $data['Alasan'],
                    'Sisa_CutiN'        => $sisa[0]['Sisa_CutiN'],
                    'Sisa_CutiN1'       => $sisa[0]['Sisa_CutiN1'],
                    'Sisa_CutiN2'       => $sisa[0]['Sisa_CutiN2'],
                    'Status'            => "Belum Diproses",
                    'Status_Enabled'    => "1",
                    'Atasan_Langsung'   => $data['Atasan_Langsung'],
                    'Kabid'             => $data['Kabid'],
                    'created_at'        => $date = Carbon::now ('Asia/Jakarta')
                ]);

                return redirect('/user.datacuti')->with('success','Pengajuan Cuti Berhasil.');
            }elseif($data['Jenis_Cuti'] == 'Tahunan' && $jumlah <= $sisacuti){
                $now = Carbon::now ('Asia/Jakarta')->format('Y');
                $tgl_mulai = date('Y', strtotime($data['Tgl_Mulai']));
                $tgl_selesai = date('Y', strtotime($data['Tgl_Selesai']));

                if ($now == $tgl_mulai && $now == $tgl_selesai){
                    Cuti::insert([
                        'IdPegawai'         => $data['IdPegawai'],
                        'NIP'               => $data['NIP'],
                        'Nama_Pegawai'      => $data['Nama_Pegawai'],
                        'Jabatan_Pegawai'   => $data['Jabatan_Pegawai'],
                        'Jenis_Pegawai'     => $data['Jenis_Pegawai'],
                        'Kategori_Pegawai'  => $data['Kategori_Pegawai'],
                        'Unit_Kerja'        => $data['Unit_Kerja'],
                        'Masa_Kerja'        => $y ." Tahun ". $m ." Bulan ". $d ." Hari ",
                        'Jenis_Cuti'        => $data['Jenis_Cuti'],
                        'Jumlah'            => $jumlah,
                        'Tgl_Mulai'         => $data['Tgl_Mulai'],
                        'Tgl_Selesai'       => $data['Tgl_Selesai'],
                        'No_Telp'           => $data['No_Telp'],
                        'Pengalihan_Kerja'  => $data['Pengalihan_Kerja'],
                        'Alasan'            => $data['Alasan'],
                        'Sisa_CutiN'        => $sisa[0]['Sisa_CutiN'],
                        'Sisa_CutiN1'       => $sisa[0]['Sisa_CutiN1'],
                        'Sisa_CutiN2'       => $sisa[0]['Sisa_CutiN2'],
                        'Status'            => "Belum Diproses",
                        'Status_Enabled'    => "1",
                        'Atasan_Langsung'   => $data['Atasan_Langsung'],
                        'Kabid'             => $data['Kabid'],
                        'created_at'        => $date = Carbon::now ('Asia/Jakarta')
                    ]);

                    return redirect('/user.datacuti')->with('success','Pengajuan Cuti Berhasil.');
                }else{
                    return redirect('/user.datacuti')->with('failed','Mohon Maaf Anda Tidak Dapat Mengajukan Cuti. Tanggal Mulai dan Akhir Cuti Harus Di Tahun Sekarang.');
                }

            }elseif($data['Jenis_Cuti'] != 'Tahunan' && $data['Jenis_Cuti'] != 'Melahirkan'){
                Cuti::insert([
                    'IdPegawai'         => $data['IdPegawai'],
                    'NIP'               => $data['NIP'],
                    'Nama_Pegawai'      => $data['Nama_Pegawai'],
                    'Jabatan_Pegawai'   => $data['Jabatan_Pegawai'],
                    'Jenis_Pegawai'     => $data['Jenis_Pegawai'],
                    'Kategori_Pegawai'  => $data['Kategori_Pegawai'],
                    'Unit_Kerja'        => $data['Unit_Kerja'],
                    'Masa_Kerja'        => $y ." Tahun ". $m ." Bulan ". $d ." Hari ",
                    'Jenis_Cuti'        => $data['Jenis_Cuti'],
                    'Jumlah'            => $jumlah,
                    'Tgl_Mulai'         => $data['Tgl_Mulai'],
                    'Tgl_Selesai'       => $data['Tgl_Selesai'],
                    'No_Telp'           => $data['No_Telp'],
                    'Pengalihan_Kerja'  => $data['Pengalihan_Kerja'],
                    'Alasan'            => $data['Alasan'],
                    'Sisa_CutiN'        => $data['Sisa_CutiN'],
                    'Sisa_CutiN1'       => $data['Sisa_CutiN1'],
                    'Sisa_CutiN2'       => $data['Sisa_CutiN2'],
                    'Status'            => "Belum Diproses",
                    'Status_Enabled'    => "1",
                    'Atasan_Langsung'   => $data['Atasan_Langsung'],
                    'Kabid'             => $data['Kabid'],
                    'created_at'        => $date = Carbon::now ('Asia/Jakarta')
                ]);

                return redirect('/user.datacuti')->with('success','Pengajuan Cuti Berhasil.');

            }
            return redirect('/user.datacuti')->with('failed','Mohon Maaf, Pengajuan Cuti Anda Tidak Dapat Diproses Karena Kuota Cuti Anda Tersisa '. $sisacuti . ' Hari');
        }else{
            return redirect('/user.datacuti')->with('failed', 'Mohon Maaf, Anda Tidak Dapat Mengajukan Cuti Karena Pengajuan Cuti Sebelumnya Belum Diproses.');
        }
    }

    //Update data cuti
    public function update(Request $request, $id)
    {
        $data = $request->all();

        //Menghitung jumlah hari
        $fdate = $request->Tgl_Mulai;
        $tdate = $request->Tgl_Selesai;
        $datetime1 = new DateTime($fdate);
        $datetime2 = new DateTime($tdate);
        $interval = $datetime2->diff($datetime1);
        $days = $interval->days;
        $jumlah = $days + 1;

        //Sisa Cuti
        if ($data['Jenis_Cuti'] == 'Tahunan'){
            $sisacuti = $data['Sisa_CutiN'] + $data['Sisa_CutiN1'] + $data['Sisa_CutiN2'];
        }

        if ($data['Jenis_Cuti'] == 'Melahirkan'){
            Cuti::where(['id'=>$id])->update([
                'Tgl_Mulai'       => date('Y-m-d', strtotime($data['Tgl_Mulai'])),
                'Tgl_Selesai'     => date('Y-m-d', strtotime('+59 days', strtotime($data['Tgl_Mulai']))),
                'No_Telp'         => $data['No_Telp'],
                'Atasan_Langsung' => $data['Atasan_Langsung'],
                'Alasan'          => $data['Alasan']
            ]);

            return redirect()->back()->with('success','Update Pengajuan Cuti Berhasil.');

        }elseif($data['Jenis_Cuti'] == 'Tahunan' && $jumlah <= $sisacuti){
            $now = Carbon::now ('Asia/Jakarta')->format('Y');
            $tgl_mulai = date('Y', strtotime($data['Tgl_Mulai']));
            $tgl_selesai = date('Y', strtotime($data['Tgl_Selesai']));

            if ($now == $tgl_mulai && $now == $tgl_selesai){
                Cuti::where(['id'=>$id])->update([
                    'Tgl_Mulai'       => date('Y-m-d', strtotime($data['Tgl_Mulai'])),
                    'Tgl_Selesai'     => date('Y-m-d', strtotime($data['Tgl_Selesai'])),
                    'Jumlah'          => $jumlah,
                    'No_Telp'         => $data['No_Telp'],
                    'Atasan_Langsung' => $data['Atasan_Langsung'],
                    'Alasan'          => $data['Alasan']
                ]);

                return redirect()->back()->with('success','Update Pengajuan Cuti Berhasil.');
            }else{
                return redirect()->back()->with('failed','Mohon Maaf Anda Tidak Dapat Mengajukan Cuti. Tanggal Mulai dan Akhir Cuti Harus Di Tahun Ini.');
            }

        }elseif($data['Jenis_Cuti'] != 'Tahunan' && $data['Jenis_Cuti'] != 'Melahirkan'){
            Cuti::where(['id'=>$id])->update([
                'Tgl_Mulai'       => date('Y-m-d', strtotime($data['Tgl_Mulai'])),
                'Tgl_Selesai'     => date('Y-m-d', strtotime($data['Tgl_Selesai'])),
                'Jumlah'          => $jumlah,
                'No_Telp'         => $data['No_Telp'],
                'Atasan_Langsung' => $data['Atasan_Langsung'],
                'Alasan'          => $data['Alasan']
            ]);

            return redirect()->back()->with('success','Update Pengajuan Cuti Berhasil.');
        }

        return redirect()->back()->with('failed','Mohon Maaf, Pengajuan Cuti Anda Tidak Dapat Diproses Karena Kuota Cuti Anda Tersisa '. $sisacuti . ' Hari');
    }

    //Delete data cuti yang masih diproses
    public function delete($id)
    {
        $cuti = Cuti::where(['id'=>$id])->update([
            'Status_Enabled'=>'0'

        ]);

        //Check data deleted or not
        if ($cuti == 1){
            $success = true;
            $message = "Data Cuti Berhasil Dihapus";
        } else {
            $success = false;
            $message = "Data Cuti Tidak Ditemukan";
        }

        //Return response
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }




    //----------- KARU CONTROLLER----------------

    //Menampilkan data cuti baru
    public function cutiKaru()
    {
        $atasan = Auth::user()->nip;
        $baru = Cuti::where([['Status', 'Belum Diproses'],['Atasan_Langsung', $atasan],['Status_Enabled', 1]])->get();
        $setuju = Cuti::where([['Status', 'Disetujui'],['Atasan_Langsung', $atasan],['Status_Enabled', 1]])->get();
        $tangguh = Cuti::where([['Status', 'Ditangguhkan'],['Atasan_Langsung', $atasan],['Status_Enabled', 1]])->get();

        return view('karu.datacuti', compact('baru', 'setuju', 'tangguh'));
    }

    public function detilKaru($id)
    {
        $detil = Cuti::where('id', $id)->get();
        $karu = Pegawai::where('NIP', $detil[0]['Atasan_Langsung'])->get();
        $kabid = Pegawai::where('NIP', $detil[0]['Kabid'])->get();

        $token = Token::where('name', 'esign')->get();

        if ($token->count() == 0)
        {
            $response = Http::post('https://apiprd.rsudtulungagung.com/api/login', [
                'email' => 'esign@iskak.com',
                'password' => 'esign@iskak.com123',
            ]);

            $responseBody = ($response->json());
            $accessToken = $responseBody['access_token'];

            Token::insert([
                'name' => 'esign',
                'token' => $responseBody['access_token'],
            ]);
        }
        return view('karu.detilcuti', compact('detil', 'karu', 'kabid'));
    }

    public function Disetujui($id)
    {
        $data = Cuti::where('id', $id)->get();
        $jenis_cuti = $data[0]['Jenis_Cuti'];
        $cuti_ambil = $data[0]['Jumlah'];
        $N          = $data[0]['Sisa_CutiN'];
        $N1         = $data[0]['Sisa_CutiN1'];
        $N2         = $data[0]['Sisa_CutiN2'];
        $SetN       = 0;
        $SetN1      = 0;
        $SetN2      = 0;
        $tahun_ambil = Carbon::now ('Asia/Jakarta')->format('Y');

        if( $jenis_cuti == 'Tahunan' ){
            if( $N2 <= 0 ){
                if( $N1 <= 0){
                    $SetN = $N - $cuti_ambil;

                    TransCuti::insert([
                        'Id_DataCuti' => $data[0]['id'],
                        'IdPegawai'   => $data[0]['IdPegawai'],
                        'Cuti_Ambil'  => $cuti_ambil,
                        'Tahun_Ambil' => $tahun_ambil,
                        'Tahun_Jatah' => $tahun_ambil,
                        'created_at'  => $date = Carbon::now ('Asia/Jakarta')
                    ]);

                    SisaCuti::where(['IdPegawai'=>$data[0]['IdPegawai']])->update([
                        'Sisa_CutiN'  => $SetN,
                        'Sisa_CutiN1' => 0,
                        'Sisa_CutiN2' => 0
                    ]);

                    Cuti::where(['id'=>$data[0]['id']])->update([
                        'Status' => 'Disetujui',
                        'Sisa_CutiN'  => $SetN,
                        'Sisa_CutiN1' => 0,
                        'Sisa_CutiN2' => 0
                    ]);
                }else{
                    $SetN1 = $N1 - $cuti_ambil;
                    if( $SetN1 < 0 ){
                        TransCuti::insert([
                            'Id_DataCuti' => $data[0]['id'],
                            'IdPegawai'   => $data[0]['IdPegawai'],
                            'Cuti_Ambil'  => $N1,
                            'Tahun_Ambil' => $tahun_ambil,
                            'Tahun_Jatah' => $tahun_ambil - 1,
                            'created_at'  => $date = Carbon::now ('Asia/Jakarta')
                        ]);

                        $SetN = $N - abs($SetN1);

                        TransCuti::insert([
                            'Id_DataCuti' => $data[0]['id'],
                            'IdPegawai'   => $data[0]['IdPegawai'],
                            'Cuti_Ambil'  => abs($SetN1),
                            'Tahun_Ambil' => $tahun_ambil,
                            'Tahun_Jatah' => $tahun_ambil,
                            'created_at'  => $date = Carbon::now ('Asia/Jakarta')
                        ]);

                        SisaCuti::where(['IdPegawai'=>$data[0]['IdPegawai']])->update([
                            'Sisa_CutiN'  => $SetN,
                            'Sisa_CutiN1' => 0,
                            'Sisa_CutiN2' => 0
                        ]);

                        Cuti::where(['id'=>$data[0]['id']])->update([
                            'Status' => 'Disetujui',
                            'Sisa_CutiN'  => $SetN,
                            'Sisa_CutiN1' => 0,
                            'Sisa_CutiN2' => 0
                        ]);
                    }else{
                        TransCuti::insert([
                        'Id_DataCuti' => $data[0]['id'],
                        'IdPegawai'   => $data[0]['IdPegawai'],
                        'Cuti_Ambil'  => $cuti_ambil,
                        'Tahun_Ambil' => $tahun_ambil,
                        'Tahun_Jatah' => $tahun_ambil - 1,
                        'created_at'  => $date = Carbon::now ('Asia/Jakarta')
                        ]);

                        SisaCuti::where(['IdPegawai'=>$data[0]['IdPegawai']])->update([
                            'Sisa_CutiN'  => $N,
                            'Sisa_CutiN1' => $SetN1,
                            'Sisa_CutiN2' => 0
                        ]);

                        Cuti::where(['id'=>$data[0]['id']])->update([
                            'Status' => 'Disetujui',
                            'Sisa_CutiN'  => $N,
                            'Sisa_CutiN1' => $SetN1,
                            'Sisa_CutiN2' => 0
                        ]);
                    }
                }
            }else{
                $SetN2 = $N2 - $cuti_ambil;
                if( $SetN2 < 0){
                    TransCuti::insert([
                        'Id_DataCuti' => $data[0]['id'],
                        'IdPegawai'   => $data[0]['IdPegawai'],
                        'Cuti_Ambil'  => $N2,
                        'Tahun_Ambil' => $tahun_ambil,
                        'Tahun_Jatah' => $tahun_ambil - 2,
                        'created_at'  => $date = Carbon::now ('Asia/Jakarta')
                    ]);

                    $SetN1 = $N1 - abs($SetN2);

                    if( $SetN1 < 0 ){
                        TransCuti::insert([
                            'Id_DataCuti' => $data[0]['id'],
                            'IdPegawai'   => $data[0]['IdPegawai'],
                            'Cuti_Ambil'  => $N1,
                            'Tahun_Ambil' => $tahun_ambil,
                            'Tahun_Jatah' => $tahun_ambil - 1,
                            'created_at'  => $date = Carbon::now ('Asia/Jakarta')
                        ]);

                        $SetN = $N - abs($SetN1);

                        TransCuti::insert([
                            'Id_DataCuti' => $data[0]['id'],
                            'IdPegawai'   => $data[0]['IdPegawai'],
                            'Cuti_Ambil'  => abs($SetN1),
                            'Tahun_Ambil' => $tahun_ambil,
                            'Tahun_Jatah' => $tahun_ambil,
                            'created_at'  => $date = Carbon::now ('Asia/Jakarta')
                        ]);

                        SisaCuti::where(['IdPegawai'=>$data[0]['IdPegawai']])->update([
                            'Sisa_CutiN'  => $SetN,
                            'Sisa_CutiN1' => 0,
                            'Sisa_CutiN2' => 0
                        ]);

                        Cuti::where(['id'=>$data[0]['id']])->update([
                            'Status' => 'Disetujui',
                            'Sisa_CutiN'  => $SetN,
                            'Sisa_CutiN1' => 0,
                            'Sisa_CutiN2' => 0
                        ]);
                    }else{
                        TransCuti::insert([
                            'Id_DataCuti' => $data[0]['id'],
                            'IdPegawai'   => $data[0]['IdPegawai'],
                            'Cuti_Ambil'  => abs($SetN2),
                            'Tahun_Ambil' => $tahun_ambil,
                            'Tahun_Jatah' => $tahun_ambil - 1,
                            'created_at'  => $date = Carbon::now ('Asia/Jakarta')
                        ]);

                        SisaCuti::where(['IdPegawai'=>$data[0]['IdPegawai']])->update([
                            'Sisa_CutiN'  => $N,
                            'Sisa_CutiN1' => $SetN1,
                            'Sisa_CutiN2' => 0
                        ]);

                        Cuti::where(['id'=>$id])->update([
                            'Status' => 'Disetujui',
                            'Sisa_CutiN'  => $N,
                            'Sisa_CutiN1' => $SetN1,
                            'Sisa_CutiN2' => 0
                        ]);
                    }

                }else{
                    TransCuti::insert([
                        'Id_DataCuti' => $data[0]['id'],
                        'IdPegawai'   => $data[0]['IdPegawai'],
                        'Cuti_Ambil'  => $cuti_ambil,
                        'Tahun_Ambil' => $tahun_ambil,
                        'Tahun_Jatah' => $tahun_ambil - 2,
                        'created_at'  => $date = Carbon::now ('Asia/Jakarta')
                    ]);

                    SisaCuti::where(['IdPegawai'=>$data[0]['IdPegawai']])->update([
                        'Sisa_CutiN'  => $N,
                        'Sisa_CutiN1' => $N1,
                        'Sisa_CutiN2' => $SetN2
                    ]);

                    Cuti::where(['id'=>$id])->update([
                        'Status' => 'Disetujui',
                        'Sisa_CutiN'  => $N,
                        'Sisa_CutiN1' => $N1,
                        'Sisa_CutiN2' => $SetN2
                    ]);
                }
            }
        }elseif( $jenis_cuti != 'Tahunan'){
            Cuti::where(['id'=>$id])->update([
                'Status'        => 'Disetujui'
            ]);
        }

        $esign_person = Pegawai::where('NIP', $data[0]['Atasan_Langsung'])->get();

        // $ip = '182.253.73.2'; //For static IP address get
        $ip = request()->ip(); //Dynamic IP address get
        $checkLocation = geoip()->getLocation($ip);
        // dd($checkLocation);

        $token = Token::where('name', 'esign')->get();

        $response = Http::withToken($token[0]['token'])->post('https://apiprd.rsudtulungagung.com/api/esign/create_esign', [
            'nik' => $esign_person[0]['NIK'],
            'nama' => $esign_person[0]['Nama_Pegawai'],
            'tgl_request' => Carbon::now ('Asia/Jakarta')->format('Y-m-d H:i:s'),
            'lokasi' => $checkLocation['lat'] .','. $checkLocation['lon'], //Koordinat
            'nama_dokumen' => 'PENGAJUAN CUTI-' . strtoupper($data[0]['Nama_Pegawai']) . '-' . $data[0]['id'],
        ]);

        $responseBody = ($response->json());
        // dd($responseBody);

        EsignCuti::insert([
            'IdCuti' => $data[0]['id'],
            'IdPegawai' => $data[0]['IdPegawai'],
            'id_dokumen' => $responseBody['response']['id_dokumen'],
            'nama_dokumen' => $responseBody['response']['dokumen'],
            'nip_esign' => $data[0]['Kabid'],
            'nama_esign' => $responseBody['response']['pemberi_tta'],
            'tgl_esign' => Carbon::now ('Asia/Jakarta')->format('Y-m-d H:i:s'),
            'lokasi' => $checkLocation['lat'] .','. $checkLocation['lon'], //Koordinat
            'esign' =>  $responseBody['response']['qrcode'],
        ]);

        return redirect()->back()->with('success','Pengajuan Cuti Berhasil Disetujui');
    }

    public function Ditangguhkan(Request $request, $id)
    {
        $data = $request->all();

        Cuti::where(['id'=>$id])->update([
            'Status'                => 'Ditangguhkan',
            'Alasan_Penangguhan'    => $data['Alasan_Penangguhan']
        ]);

        return redirect()->back()->with('success','Pengajuan Cuti Telah Ditangguhkan');
    }


    //----------- KABID CONTROLLER----------------

    //Menampilkan data cuti baru
    public function cutiKabid()
    {
        $kabid = Auth::user()->nip;
        $ttd = Cuti::where([['Status', 'Disetujui'],['Kabid', $kabid], ['TTD', NULL], ['Status_Enabled', 1]])->get();

        return view('karu.datacuti', compact('ttd'));
    }

}
