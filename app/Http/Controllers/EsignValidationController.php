<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;

class EsignValidationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function capthcaFormValidate(Request $request)
    {
        // dd($request->captcha);

        $validator = Validator::make($request->all(), [
            'captcha' => 'required|captcha',
        ]);

        if ($validator->fails()) {
            echo '<p style="color: #ff0000;">Incorrect!</p>';
        } else {
            echo '<p style="color: #00ff30;">Matched :)</p>';
        }
    }

    public function reloadCaptcha()
    {
        return response()->json(['captcha' => captcha_img('math')]);
    }
    //
    public function getEsign($id_dokumen)
    {
        $response = Http::get('http://127.0.0.1:8008/api/esign/' . $id_dokumen);

        $responseBody = ($response->json());
        // dd($responseBody);

        return view('data.data-esign', compact('responseBody'));
    }
}
