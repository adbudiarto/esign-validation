@extends('app')
@section('content')

    <div class="inner-bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 text">
                    {{-- <h1><strong>Bootstrap</strong> Login Form</h1>
                    <div class="description">
                        <p>
                            This is a free responsive login form made with Bootstrap.
                            Download it <a href="https://github.com/AZMIND" target="_blank"><strong>here</strong></a>,
                            customize and use it as you like!
                        </p> --}}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 form-box">
                <div class="form-top">
                    <div class="form-top-left">
                        <h3>Login to our site</h3>
                        <p>Enter your username and password to log on:</p>
                    </div>
                    <div class="form-top-right">
                        <i class="fa fa-lock"></i>
                    </div>
                </div>
                <div class="form-bottom">
                    <form action="/captcha-validation" method="POST">
                        @csrf
                        <div style="margin-left: 400px;">
                           <div class="row">
                           <div class="form-group row">
                               <div class="col-xs-12 col-sm-12 col-md-12">
                                   {!! NoCaptcha::display() !!}
                                   {!! NoCaptcha::renderJs() !!}
                                   {!! htmlFormSnippet() !!}
                                   @error('g-recaptcha-response')
                                   <span class="text-danger" role="alert">
                                       <strong>{{ $message }}</strong>
                                   </span>
                                   @enderror
                               </div>
                           </div>
                           <div class="row">
                              <div class="col-xs-12 col-sm-12 col-md-12 text-left">
                                 <button type="submit" class="btn btn-primary">Submit</button>
                              </div>
                           </div>
                        </div>
                     </form>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 social-login">
                {{-- <h3>...or login with:</h3>
                <div class="social-login-buttons">
                    <a class="btn btn-link-1 btn-link-1-facebook" href="#">
                        <i class="fa fa-facebook"></i> Facebook
                    </a>
                    <a class="btn btn-link-1 btn-link-1-twitter" href="#">
                        <i class="fa fa-twitter"></i> Twitter
                    </a>
                    <a class="btn btn-link-1 btn-link-1-google-plus" href="#">
                        <i class="fa fa-google-plus"></i> Google Plus
                    </a>
                </div> --}}
            </div>
        </div>
    </div>
    </div>
