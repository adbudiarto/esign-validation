@extends('app')
@section('content')

    <div class="inner-bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 text">
                    {{-- <h1><strong>Bootstrap</strong> Login Form</h1>
                    <div class="description">
                        <p>
                            This is a free responsive login form made with Bootstrap.
                            Download it <a href="https://github.com/AZMIND" target="_blank"><strong>here</strong></a>,
                            customize and use it as you like!
                        </p> --}}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 form-box">
                <div class="form-top">
                    <div class="form-top-left">
                        <img src="assets/img/logo-rsdi.png" style="height: 50; width: auto">
                    </div>
                    <div class="form-top-right">
                        <i class="fa fa-lock"></i>
                    </div>
                </div>
                <div class="form-bottom">
                    @if ($responseBody['response']['status'] === 'VALID')
                        <div class="logo">
                            <center><img src="{{ url('assets/img/valid.png') }}"></center>
                        </div>
                        <div class="row" style="margin: 0px 100px 0px;">
                            <hr>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <b>Status</b>
                                <br>
                                <b style="color: green">
                                    <?php echo $responseBody['response']['status']; ?>
                                </b>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12 mb-2">
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12 mb-2">
                                <b>Nama Dokumen</b>
                                <br>
                                <?php echo $responseBody['response']['dokumen']; ?>
                            </div>
                        </div>
                        <div class="row" style="margin: 30px 100px 0px;">
                            <b style="color:blue">Info Penandatanganan</b>
                            <hr>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <b>Penandatangan</b>
                                <br>
                                <?php echo $responseBody['response']['pemberi_tta']; ?>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <b>Lokasi</b>
                                <br>
                                <?php
                                function getaddress($lat, $lng)
                                {
                                    $url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' . trim($lat) . ',' . trim($lng) . '&sensor=false';
                                    $json = @file_get_contents($url);
                                    $data = json_decode($json);
                                    $status = $data->status;
                                    if ($status == 'OK') {
                                        return $data->results[0]->formatted_address;
                                    } else {
                                        return false;
                                    }
                                }

                                $lat = substr($responseBody['response']['lokasi'], 0, -7);
                                $lng = substr($responseBody['response']['lokasi'], 6);
                                $address = getaddress($lat, $lng);
                                if ($address) {
                                    echo $address;
                                } else {
                                    echo '-';
                                }
                                ?>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <b>Tanggal</b>
                                <br>
                                <?php echo $responseBody['response']['tgl_tta']; ?>
                            </div>
                        </div>
                        <div class="row" style="margin: 30px 100px 100px;">
                            <b style="color:blue">Info Tambahan</b>
                            <hr>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <b>Tanggal Pindai</b>
                                <br>
                                <?php
                                date_default_timezone_set('Asia/Jakarta');
                                echo date('Y-d-m H:i:s');
                                ?>
                            </div>
                        </div>
                        <div class="form-top">
                            <div class="row">
                                <p></p>
                                <div class="col-md-2 col-sm-4 col-xs-12">
                                    <img src="{{ url('assets/img/logo-ipti.png') }}" style="size: 10%">
                                </div>
                                <div class="col-md-10 col-sm-4 col-xs-12">
                                    <b style="color: rgb(56, 56, 56);">
                                        Dokumen ini telah ditandatangani secara elektronik
                                        menggunakan eSign milik RSUD dr. Iskak Tulungagung</b>
                                </div>
                            </div>
                        </div>
                    @elseif($responseBody['response']['status'] === 'TIDAK VALID')
                        <div class="logo">
                            <center><img src="{{ url('assets/img/invalid.png') }}"></center>
                        </div>
                        <div class="row" style="margin: 0px 100px 175px;">
                            <hr>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <center>
                                    <div class="form-group">
                                        <b>Status</b>
                                        <br>
                                        <b style="color: red">
                                            <?php echo $responseBody['response']['status']; ?>
                                        </b>
                                    </div>
                                </center>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 social-login">
                <strong>Copyright IPTI &copy; <?php echo date('Y'); ?> eSign Team.</strong> All rights reserved.
            </div>
        </div>
    </div>
    </div>
